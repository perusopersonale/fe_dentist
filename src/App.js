import './App.css';
import Login from './Login';
import Calendar from './Calendar';
import useAppState from './useAppState'
import { CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

const useStyles = makeStyles((theme) => ({
 loading: {
  width: "100vw",
  height: "100vh",
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
 }
}));

function App() {
  const appState = useAppState()
  const classes = useStyles()
  const { user, initialLoading } = appState
  return (

    <>
     <ToastContainer
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
      />
      { initialLoading && <div className={classes.loading}><CircularProgress size={100}/></div>}
      {!initialLoading && !user && <Login {...appState} /> }
      {!initialLoading && user && <Calendar {...appState} />}
    </>
  )
}

export default App;
