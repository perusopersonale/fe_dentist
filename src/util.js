export const extractResponseError = (err) => {
  // eslint-disable-next-line
  return err && err.response && err.response.data && err.response.data.error || "Unknown error"
}