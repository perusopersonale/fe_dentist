import React, { useCallback, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios'
import { toast } from 'react-toastify';
import { extractResponseError } from './util';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function RegisterForm({ displayLogin }) {
  const classes = useStyles();
  const [email, setEmail ] = useState("")
  const [password, setPassword ] = useState("")
  const [firstName, setFirstName ] = useState("")
  const [lastName, setLastName ] = useState("")
  const [loading, setLoading ] = useState(false)
  const register = useCallback( async () => {
    try{

      await axios.post('/api/users', {
        email, password, firstName, lastName
      });
      displayLogin()
    } catch(err){
      const error =  extractResponseError(err)
      toast.error(`An error occured while registering: ${error}`);
    }
    finally{
      setLoading(false)
    }
  },[email, password, firstName, lastName, displayLogin])

  return (<>
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        Register 
      </Typography>
  
      <form className={classes.form} noValidate onSubmit={(e)=>e.preventDefault()}>
      <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="firstName"
          label="First Name"
          name="firstName"
          autoFocus
          disabled={loading}
          onChange={(e)=> setFirstName(e.currentTarget.value)}
          value={firstName}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="lastName"
          label="Last Name"
          name="lastName"
          disabled={loading}
          onChange={(e)=> setLastName(e.currentTarget.value)}
          value={lastName}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          autoFocus
          disabled={loading}
          onChange={(e)=> setEmail(e.currentTarget.value)}
          value={email}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          disabled={loading}
          onChange={(e)=> setPassword(e.currentTarget.value)}
          value={password}
        />
        
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          disabled={loading}
          onClick={()=> register()}
        >
          Sign In
        </Button>
        <Grid container>
          <Grid item>
            <Link href="#" onClick={() =>displayLogin()} variant="body2">
              {"Already registered? Sign Up"}
            </Link>
          </Grid>
        </Grid>
      </form>
    </>
  );
}