import React from 'react';
import {
  WeekView,
} from '@devexpress/dx-react-scheduler-material-ui';

export default function TimeTableCell (props)  {
  const { startDate, endDate, hiddenAppointments = [] } = props;
  const otherProps = {}
  const isCellDisabled =  hiddenAppointments.find( app =>{
    const appStartDate = new Date(app.startDate)
    const appEndDate = new Date(app.endDate)
    
    // eslint-disable-next-line
    return appStartDate >= startDate && appStartDate < endDate || appEndDate > startDate && appEndDate < endDate
  })
  
  if(isCellDisabled) otherProps.onDoubleClick = undefined
  return <WeekView.TimeTableCell {...props} {...otherProps} isShaded={isCellDisabled} />;
};