import React from 'react';
import Paper from '@material-ui/core/Paper';
import { ViewState, EditingState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  WeekView,
  Appointments,
  DateNavigator,
  Toolbar,
  EditRecurrenceMenu,
  ConfirmationDialog,
  AppointmentTooltip,
  AppointmentForm,
} from '@devexpress/dx-react-scheduler-material-ui';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import Header from './Header'
import axios from 'axios'
import { connectProps } from '@devexpress/dx-react-core';
import FormAppointment from './FormAppointment';
import TimeTableCell from './TimeTableCell';
import ToolbarWithLoading from './ToolbarLoading';
import { extractResponseError } from './util';
import { toast } from 'react-toastify';

const appointmentsEndpoint = "/api/appointments"

class Calendar extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      currentDate: new Date(),
      confirmationVisible: false,
      editingFormVisible: false,
      deletedAppointmentId: undefined,
      editingAppointment: undefined,
      previousAppointment: undefined,
      addedAppointment: {},
      startDayHour: 9,
      endDayHour: 20,
      isNewAppointment: false,
      appointments: [],
      hiddenAppointments: [],
      loading: false
    };

    this.toggleConfirmationVisible = this.toggleConfirmationVisible.bind(this);
    this.commitDeletedAppointment = this.commitDeletedAppointment.bind(this);
    this.toggleEditingFormVisibility = this.toggleEditingFormVisibility.bind(this);

    this.commitChanges = this.commitChanges.bind(this);
    this.onEditingAppointmentChange = this.onEditingAppointmentChange.bind(this);
    this.onAddedAppointmentChange = this.onAddedAppointmentChange.bind(this);

    this.getAppointments = this.getAppointments.bind(this);
    this.postAppointment = this.postAppointment.bind(this);
    this.putAppointment = this.putAppointment.bind(this);
    this.deleteAppointment = this.deleteAppointment.bind(this);

    this.onCurrentDateChange = this.onCurrentDateChange.bind(this);

    this.appointmentForm = connectProps(FormAppointment, () => {
      const {
        editingFormVisible,
        editingAppointment,
        data,
        addedAppointment,
        isNewAppointment,
        previousAppointment,
      } = this.state;

      const currentAppointment = data
        .filter(appointment => editingAppointment && appointment.id === editingAppointment.id)[0]
        || addedAppointment;
      const cancelAppointment = () => {
        if (isNewAppointment) {
          this.setState({
            editingAppointment: previousAppointment,
            isNewAppointment: false,
          });
        }
      };

      return {
        visible: editingFormVisible,
        appointmentData: currentAppointment,
        commitChanges: this.commitChanges,
        visibleChange: this.toggleEditingFormVisibility,
        onEditingAppointmentChange: this.onEditingAppointmentChange,
        cancelAppointment,
      };
    });
    this.timeTableCellComponent = connectProps(TimeTableCell, () => {
      const {
        hiddenAppointments,
      } = this.state;
      return { hiddenAppointments}
    });
  }
  

  componentDidUpdate() {
    this.appointmentForm.update();
  }
  componentDidMount() {
    this.getAppointments(this.state.currentDate);
  }
  onCurrentDateChange(currentDate) {
    this.setState({ currentDate })
    this.getAppointments(currentDate)
  } 

  onEditingAppointmentChange(editingAppointment) {
    this.setState({ editingAppointment });
  }  

  onAddedAppointmentChange(addedAppointment) {
    this.setState({ addedAppointment });
    const { editingAppointment } = this.state;
    if (editingAppointment !== undefined) {
      this.setState({
        previousAppointment: editingAppointment,
      });
    }
    this.setState({ editingAppointment: undefined, isNewAppointment: true });
  }

  setDeletedAppointmentId(id) {
    this.setState({ deletedAppointmentId: id });
  }

  toggleEditingFormVisibility() {
    const { editingFormVisible } = this.state;
    this.setState({
      editingFormVisible: !editingFormVisible,
    });
  }

  toggleConfirmationVisible() {
    const { confirmationVisible } = this.state;
    this.setState({ confirmationVisible: !confirmationVisible });
  }

  async commitDeletedAppointment() {
    const { data, deletedAppointmentId } = this.state
    try {
      await this.deleteAppointment(deletedAppointmentId)
      const nextData = data.filter(appointment => appointment.id !== deletedAppointmentId);
      this.setState({data: nextData })
      toast.success("Delete complete")
    } catch (err){
      const error = extractResponseError(err)
      toast.error(error)
    }
    this.toggleConfirmationVisible();
  }
  async getAppointments(date){
    try {
      const params = new URLSearchParams({
        start: [ date.toUTCString() ]
      });
      this.setState({loading: true})
      const res = await axios.get(`${appointmentsEndpoint}?${params.toString()}`)
      const [ userAppointments, otherAppointments ] = res.data.data.reduce( (out, app) =>{
        //dividind appointments owned by user and other  
        return app.userId ? [ [...out[0],app], out[1] ]: [ out[0], [...out[1], app]]
      } ,[[],[]])
      this.setState({ data: userAppointments, hiddenAppointments: otherAppointments })
    } catch(err){
      const error = extractResponseError(err)
      toast.error(error)
    }
    finally{
      this.setState({loading: false})
    }
  }
  async postAppointment({ title, startDate, endDate, description }){
    const res = await axios.post(appointmentsEndpoint, { title, startDate, endDate, description })
    return res.data.data 
  }
  async putAppointment(id, { title, startDate, endDate, description }){
    const res = await axios.put(`${appointmentsEndpoint}/${id}`,{ title, startDate, endDate, description })
    return res.data.data.id
  }
  async deleteAppointment(id){
    await axios.delete(`${appointmentsEndpoint}/${id}`)
  }
  async commitChanges({ added, changed, deleted }) {
    const { data } = this.state
    let nextData 
    try {
      if (added) {
        const id = await this.postAppointment(added)
        nextData = [...data, { id, ...added }];
        this.setState({ data: nextData, addedAppointment: {} });
        toast.success("Created new appointment")
      }
      if (changed) {
        const { id, ...changes} = changed
        await this.putAppointment(id, changes)
        nextData = data.map(appointment => appointment.id === id ? { ...appointment, ...changes } : appointment);
        this.setState({ data: nextData, addedAppointment: {} });
        toast.success("Appointment edit")
      }
      if (deleted) {
        this.setDeletedAppointmentId(deleted);
        this.toggleConfirmationVisible();
      }
    } catch (err){
      const error = extractResponseError(err)
      toast.error(error)
    }
  }

  render() {
    const {
      currentDate,
      data,
      confirmationVisible,
      editingFormVisible,
      startDayHour,
      endDayHour,
      loading
    } = this.state;
    
    return <>
    <Header {...this.props}/>
      
    <Container component="main">
        <CssBaseline />
        <Paper>
          <Scheduler
            data={data}
          >
            <ViewState 
              currentDate={currentDate}
              onCurrentDateChange={ this.onCurrentDateChange}
            />
            <EditingState
              onCommitChanges={this.commitChanges}
              onEditingAppointmentChange={this.onEditingAppointmentChange}
              onAddedAppointmentChange={this.onAddedAppointmentChange}
            />
            <WeekView
              startDayHour={startDayHour}
              endDayHour={endDayHour}
              excludedDays={[0,6]}
              timeTableCellComponent={
                connectProps(TimeTableCell, () => {
                  const {
                    hiddenAppointments,
                  } = this.state;
                  return { hiddenAppointments}
                })
              }
            />
            <Toolbar
              {...loading ? { rootComponent: ToolbarWithLoading } : null}
            />
            <DateNavigator />
            <EditRecurrenceMenu />
            <ConfirmationDialog />
            <Appointments />
            
            <AppointmentTooltip
              showOpenButton
              showCloseButton
            />
            <AppointmentForm
              overlayComponent={this.appointmentForm}
              visible={editingFormVisible}
              onVisibilityChange={this.toggleEditingFormVisibility}
            />
          </Scheduler>
          <Dialog
            open={confirmationVisible}
            onClose={this.cancelDelete}
          >
            <DialogTitle>
              Delete Appointment
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                Are you sure you want to delete this appointment?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.toggleConfirmationVisible} color="primary" variant="outlined">
                Cancel
              </Button>
              <Button onClick={this.commitDeletedAppointment} color="secondary" variant="outlined">
                Delete
              </Button>
            </DialogActions>
          </Dialog>
        </Paper>
      </Container>
  </>
  }
}

export default Calendar;