import axios from "axios"
import { useEffect, useState } from "react"

export default function useAppSate() {
  const [ user, setUser ] = useState()
  const [ initialLoading, setInitialLoading ] = useState(true)
  useEffect(()=> {
    async function getProfile(){
      try {
        const res = await axios.get('/api/profile')
        setUser(res.data.data)
      }catch(err){ 
      }
      finally{
        setInitialLoading(false)
      }
    }
    getProfile()
  },[])
  return {
    user,
    initialLoading,
    setUser
  }
}