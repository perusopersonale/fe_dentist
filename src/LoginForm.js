import React, { useCallback, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios'
import { extractResponseError } from './util';
import { toast } from 'react-toastify';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function LoginForm({ displayRegister, setUser}) {
  const classes = useStyles();
  const [email, setEmail ] = useState("")
  const [password, setPassword ] = useState("")
  const [loginErr, setLoginErr ] = useState(false)
  const [loading, setLoading ] = useState(false)
  const login = useCallback( async () => {
    try {
      const res = await axios.post("/api/auth/login", {
       email, password
      });
      setUser(res.data.data)
    } catch(err){
      extractResponseError(err)
      toast.error(err)
    }
    finally{
      
      setLoading(false)
    }
  },[email, password, setUser])

  return (<>
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        Sign in
      </Typography>
      { loginErr && <Typography component="h1" variant="body1">{loginErr}</Typography>}
      <form className={classes.form} noValidate onSubmit={(e)=>e.preventDefault()}>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          autoFocus
          disabled={loading}
          onChange={(e)=>{
            setEmail(e.currentTarget.value)
            setLoginErr(null)
          }}
          value={email}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          disabled={loading}
          onChange={(e)=>{
            setPassword(e.currentTarget.value)
            setLoginErr(null)
          }}
          value={password}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          disabled={loading}
          onClick={()=> login()}
        >
          Sign In
        </Button>
        <Grid container>
          <Grid item>
            <Link href="#" onClick={()=>displayRegister()} variant="body2">
              {"Don't have an account? Sign Up"}
            </Link>
          </Grid>
        </Grid>
      </form>
    </>
  );
}